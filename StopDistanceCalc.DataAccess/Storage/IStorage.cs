﻿using System;
using System.Collections.Generic;
using StopDistanceCalc.DataAccess.Entity;

namespace StopDistanceCalc.DataAccess.Storage
{
    public interface IStorage
    {
        event EventHandler DataChanged;
        List<StopEntity> LoadData();
    }
}
