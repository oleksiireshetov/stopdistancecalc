﻿using System;

namespace StopDistanceCalc.DataAccess.Entity
{
    public class StopEntity
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
