﻿(function () {
    'use strict';

    angular
        .module('sdcApp')
        .controller('stopController', stopController);

    stopController.$inject = ['$scope', 'stopService', '$parse'];

    function stopController($scope, stopService, $parse) {

        $scope.startStop = {};
        $scope.endStop = {};

        $scope.refreshStops = function(stopName, list) {
            stopService.getData(stopName).then(function (data) {
                var model = $parse(list);
                model.assign($scope, data);
            });
        };

        $scope.calculate = function(startStop, endStop) {
            if (startStop == undefined || endStop == undefined) {
                return;
            }
            $scope.distance = stopService.calculate(startStop, endStop);
        }
    }
})();
