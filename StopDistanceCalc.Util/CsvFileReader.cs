﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using StopDistanceCalc.Util.Exceptions;

namespace StopDistanceCalc.Util
{
    /// <summary>
    /// Provides access to a CSV file and mapping data from file to object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CsvFileReader<T> : IDisposable where T: new()
    {
        private readonly StreamReader _reader;
        private bool _disposed = false;

        /// <summary>
        /// Initializes a new instance of the StreamReader class for the specified file
        /// </summary>
        /// <remarks>
        /// The constructor initialize object and set property that CSV file has header line. 
        /// </remarks>
        /// <param name="filePath">The complete file path to be read</param>
        public CsvFileReader(string filePath) : this(filePath, true) { }
        /// <summary>
        /// Initializes a new instance of the StreamReader class for the specified file
        /// </summary>
        /// <param name="filePath">The complete file path to be read</param>
        /// <param name="hasHeader">The parameter specifies whether a file has header or not</param>
        public CsvFileReader(string filePath, bool hasHeader)
        {
            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            _reader = new StreamReader(fileStream);
            if (hasHeader)
            {
                _reader.ReadLine();
            }
        }

        /// <summary>
        /// Reads a line from the file and maps data to an object of type T
        /// </summary>
        /// <returns>Object of type T</returns>
        public T ReadLine()
        {
            string line = _reader.ReadLine();
            if (line == null)
            {
                return default(T);
            }
            var values = line.Split(',');
            var result = new T();
            var i = 0;
            var properties = typeof (T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in properties)
            {
                prop.SetValue(result, ChangeType(values[i++], prop), null);
            }
            return result;
        }

        /// <summary>
        /// Reads all lines the file, maps data to an object of type T and return list of objects
        /// </summary>
        /// <returns>List of objects</returns>
        public IEnumerable<T> ReadToEnd()
        {
            var result = new List<T>();
            var item = ReadLine();

            while (item != null)
            {
                result.Add(item);
                item = ReadLine();
            }
            return result;
        }

        /// <summary>
        /// Changes type from string to type of property
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="prop">Property</param>
        /// <returns>Converted value</returns>
        private static object ChangeType(string value, PropertyInfo prop)
        {
            object result = null;
            if (Type.GetTypeCode(prop.PropertyType) == TypeCode.String)
            {
                value = value.Trim('"');
            }
            try
            {
                result = Convert.ChangeType(value, prop.PropertyType);
            }
            catch (FormatException)
            {
                throw new InvalidMappingException(String.Format("Cannot map '{0}' value to property '{1}' of '{2}' type", value, prop.Name, prop.PropertyType));
            }
            return result;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _reader?.Close();
            }
            _disposed = true;
        }
    }

}
