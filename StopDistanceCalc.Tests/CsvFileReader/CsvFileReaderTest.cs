﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using StopDistanceCalc.Tests.CsvFileReader;
using StopDistanceCalc.Util;
using StopDistanceCalc.Util.Exceptions;

namespace StopDistanceCalc.Tests
{
    [TestFixture]
    public class CsvFileReaderTest
    {
        [Test]
        public void InitializeReaderWithInvalidaFileName_ThrowsException()
        {
            Assert.Throws(typeof(FileNotFoundException), InitializeReaderWithInvalidaFileName);
        }

        [Test]
        public void ReadToEndWithHeader_HeaderShouldBeSkipped()
        {
            var reader = new CsvFileReader<Client>(@"testdata\td1.csv");
            var result = reader.ReadToEnd();
            /*
            Test data contains 11 rows - 1 row for header and rest for data
            ReadToEnd should return only 10 records 
            */
            Assert.AreEqual(10, result.Count());
        }

        [Test]
        public void MapDataToWrongProperties_ShouldFail()
        {
            Assert.Throws(typeof(InvalidMappingException), MapDataToWrongProperties, "Cannot map 'id' value to property 'Id' of 'System.Int32' type");
        }

        private void MapDataToWrongProperties()
        {
            var reader = new CsvFileReader<VexClient>(@"testdata\td1.csv", false);
            var result = reader.ReadToEnd();
        }

        private void InitializeReaderWithInvalidaFileName()
        {
            var reader = new CsvFileReader<object>("this_file_doesn't_exist.no_extension_at_all");
        }
    }
}
