﻿function calculate() {
    var startStopId = $('#StartStopId').val()
    var endStopId = $("#EndStopId").val();
    $.when($.ajax({ url: "/api/stops/" + startStopId }), $.ajax({ url: "/api/stops/" + endStopId })).done(function (startStopResult, endStopResult) {
        var result = getDistanceFromLatLonInKm(
            startStopResult[0].Latitude, startStopResult[0].Longitude, 
            endStopResult[0].Latitude, endStopResult[0].Longitude
            );
        $("#result").html("Distance beetween " + startStopResult[0].Name + " and " + endStopResult[0].Name + ": " + result);
        $("#result").show();

    });
}



function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180);
}