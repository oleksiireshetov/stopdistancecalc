﻿using System.Collections.Generic;
using StopDistanceCalc.DataAccess.Entity;

namespace StopDistanceCalc.DataAccess.Repository
{
    public interface IStopRepository
    {
        List<StopEntity> GetAll();
        StopEntity GetById(int id);
        List<StopEntity> GetByQuery(string query);
    }
}
