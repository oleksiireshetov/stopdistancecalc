﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using StopDistanceCalc.DataAccess.Entity;
using StopDistanceCalc.Util;

namespace StopDistanceCalc.DataAccess.Storage
{
    public class FileStorage : IStorage
    {
        private readonly String _filePath;
        private static FileSystemWatcher _watcher;
        public FileStorage(string filePath)
        {
            _filePath = filePath;
            AssignWatcher();
        }

        private void AssignWatcher()
        {
            if (_watcher == null)
            {
                string directory = Path.GetPathRoot(_filePath);
                string file = Path.GetFileName(_filePath);

                _watcher = new FileSystemWatcher(directory, file)
                {
                    NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.LastWrite,
                    EnableRaisingEvents = true
                };
                _watcher.Changed += Watcher_Changed;
            }
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            DataChanged?.Invoke(this, null);
        }

        public event EventHandler DataChanged;

        public List<StopEntity> LoadData()
        {
            List<StopEntity> stops;
            using (var reader = new CsvFileReader<StopEntity>(_filePath))
            {
                stops = reader.ReadToEnd().ToList();
            }
            return stops;
        }
    }
}
