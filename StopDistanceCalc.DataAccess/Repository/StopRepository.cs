﻿using System;
using System.Collections.Generic;
using System.Linq;
using StopDistanceCalc.DataAccess.Entity;
using StopDistanceCalc.DataAccess.Storage;

namespace StopDistanceCalc.DataAccess.Repository
{
    public class StopRepository : IStopRepository
    {
        static List<StopEntity> _stops;
        static IStorage _storage;


        public StopRepository(IStorage storage)
        {
            _storage = storage;
            if (_stops == null)
            {
                _storage.DataChanged += _storage_DataChanged;
                _stops = _storage.LoadData();
            }
        }

        private void _storage_DataChanged(object sender, EventArgs e)
        {
            _stops = _storage.LoadData();
        }

        public List<StopEntity> GetAll()
        {
            return _stops;
        }
        public StopEntity GetById(int id)
        {
            return _stops.FirstOrDefault(x => x.Id == id);
        }

        public List<StopEntity> GetByQuery(string query)
        {
            return _stops.Where(x => x.Name.ToLower().Contains(query.ToLower())).ToList();
        }
    }
}
