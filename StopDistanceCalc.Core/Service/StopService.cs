﻿using System;
using System.Collections.Generic;
using System.Linq;
using StopDistanceCalc.Core.Model;
using StopDistanceCalc.DataAccess.Entity;
using StopDistanceCalc.DataAccess.Repository;

namespace StopDistanceCalc.Core.Service
{

    /*
    //TODO: Replace MapEntity with Automapper (http://automapper.org/) 
    Visual Studio 2015 RC crashes in case of adding Autommaper via NuGet
    */

    /// <summary>
    /// StopService mediates between data access layer and businnes layer
    /// </summary>
    public class StopService : IStopService
    {
        private readonly IStopRepository _stopRepository;
        private readonly Func<StopEntity, Stop> _mapper ;


        /// <summary>
        /// Maps a data entity to business entity
        /// </summary>
        /// <param name="stopEntity">Data entity</param>
        /// <returns>Business Entity</returns>
        private Stop MapEntity(StopEntity stopEntity)
        {
            return new Stop
            {
                Description = stopEntity.Description,
                Id = stopEntity.Id,
                Latitude = stopEntity.Latitude,
                Longitude = stopEntity.Longitude,
                Name = stopEntity.Name
            };
        }
        public StopService(IStopRepository stopRepository)
        {
            _mapper = MapEntity;
            _stopRepository = stopRepository;
        }

        /// <summary>
        /// Get all stops
        /// </summary>
        /// <returns>List of stops</returns>
        public List<Stop> GetAll()
        {
            return _stopRepository.GetAll().Select(_mapper).ToList();
        }

        /// <summary>
        /// Get stop by ID
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>Stop object</returns>
        public Stop GetById(int id)
        {
            return _mapper(_stopRepository.GetById(id));
        }

        /// <summary>
        /// Gets stops by query
        /// </summary>
        /// <param name="query">Search string</param>
        /// <returns>List of stops</returns>
        public List<Stop> GetByQuery(string query)
        {
            return _stopRepository.GetByQuery(query).Select(_mapper).ToList();
        }
    }
}
