﻿using System.Web.Mvc;
using StopDistanceCalc.Core.Service;
using StopDistanceCalc.DataAccess.Repository;

namespace StopDistanceCalc.Web.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

  
    }
}