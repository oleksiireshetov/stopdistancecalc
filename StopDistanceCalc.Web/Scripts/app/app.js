﻿(function () {
    'use strict';

    var sdcApp = angular.module('sdcApp', ['ngRoute', 'ui.select', 'ngSanitize', 'ngStorage']);

    sdcApp.config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: '/Scripts/app/views/index.html'
                }).
                when('/calculator', {
                    templateUrl: '/Scripts/app/views/calculator.html',
                    controller: 'stopController'
                }).
                otherwise({
                    redirectTo: '/'
                });
        }
    ]);
})();