﻿using System.Collections.Generic;
using StopDistanceCalc.Core.Model;

namespace StopDistanceCalc.Core.Service
{
    public interface IStopService
    {
        List<Stop> GetAll();
        Stop GetById(int id);
        List<Stop> GetByQuery(string query);
    }
}
