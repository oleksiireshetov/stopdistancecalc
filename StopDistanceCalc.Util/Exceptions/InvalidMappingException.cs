﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopDistanceCalc.Util.Exceptions
{
    public class InvalidMappingException : Exception
    {
        public InvalidMappingException(string message) : base(message)
        {

        }
    }
}
