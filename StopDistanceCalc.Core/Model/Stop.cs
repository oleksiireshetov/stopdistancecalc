﻿using System;

namespace StopDistanceCalc.Core.Model
{
    /// <summary>
    /// Describes Stop model
    /// </summary>
    public class Stop
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
