﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using StopDistanceCalc.Core.Model;
using StopDistanceCalc.Core.Service;

namespace StopDistanceCalc.Web.Controllers
{
    public class StopApiController : ApiController
    {
        readonly IStopService _stopService;

        public StopApiController(IStopService stopService)
        {
            _stopService = stopService;
        }
        [HttpGet]
        [Route("api/stops/{id}")]
        public IHttpActionResult GetStop(int id)
        {
            var result = _stopService.GetById(id);
            return Json<Stop>(result);
        }

        [HttpGet]
        [Route("api/stops/search")]
        public IHttpActionResult SearchStops(string searchQuery)
        {
            var result = _stopService.GetByQuery(searchQuery);
            return Json(result.ToList());
        }

    }
}
