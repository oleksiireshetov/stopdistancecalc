using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using StopDistanceCalc.Core.Service;
using StopDistanceCalc.DataAccess.Repository;
using StopDistanceCalc.DataAccess.Storage;

namespace StopDistanceCalc.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            //If Azure key is TRUE application will map path inside application root.
            //CSV_FilePath and Azure keys are overridden in Web.Release.Config 
            //TODO: Use bool.TryParse method to make sure that Azure value is true/false
            //TODO: Add check that configuration keys exist
            var isAzureEnv = WebConfigurationManager.AppSettings["Azure"];
            var fileName = bool.Parse(isAzureEnv) ? 
                HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["CSV_FilePath"]) : WebConfigurationManager.AppSettings["CSV_FilePath"];
          
            //Pass fileName to FileStorage constructor
            container.RegisterType<IStorage, FileStorage>(new InjectionConstructor(fileName));

            //Register Types
            container.RegisterType<IStopRepository, StopRepository>();
            container.RegisterType<IStopService, StopService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}