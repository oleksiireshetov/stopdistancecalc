﻿(function() {
    'use strict';

    angular
        .module('sdcApp')
        .factory('stopService', stopService);

    stopService.$inject = ['$http', '$q'];

    function stopService($http, $q) {

        var deg2rad = function (deg) {
            return deg * (Math.PI / 180);
        }

        var getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2 - lat1); // deg2rad below
            var dLon = deg2rad(lon2 - lon1);
            var a =
                Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon / 2) * Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c; // Distance in km
            return d;
        }


        var service = {
            getData: function (query) {
                var deferred = $q.defer();
                if (!query) {
                    return deferred.promise;
                }
                var escapedQuery = encodeURIComponent(query);
                deferred.promise = $http.get('/api/stops/search?searchQuery=' + escapedQuery).then(function (response) {
                    return response.data;
                });
                return deferred.promise;;
            },

            calculate: function(startStop, endStop) {
                var distance = getDistanceFromLatLonInKm(startStop.Latitude, startStop.Longitude, endStop.Latitude, endStop.Longitude);
                var result = "Distance " + distance.toFixed(2) + ' km';
                return result;
            }
        };

        return service;
    }
})();